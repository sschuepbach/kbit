# Persönliche Wissensdatenbank IT

**https://kbit.annotat.io**

## Setup

Die Wissensdatenbank besteht aus Markdown-Dateien im `docs/`-Verzeichnis, welche mit [Hugo](https://gohugo.io) als Website generiert werden. Das verwendete Theme ist [hugo-book](https://github.com/alex-shpak/hugo-book).

## TODOs Migration

* [x] Titel in Front Matter setzen
* [x] Textuelle Datumsangaben wo vorhanden in Front Matter übertragen
* [x] Titelsyntax anpassen
* [ ] Schlagworte in Front Matter aufnehmen
* [ ] Obsolete und leere Artikel löschen
* [ ] Codeblocks mit Language-Tags versehen
