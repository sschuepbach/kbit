# Wissensdatenbank IT

{{< hint warning >}}
Ich bin im Moment daran, die Wissensdatenbank auf Hugo zu migrieren.
Insbesondere mit Formatierungsfehlern ist noch zu rechnen.
{{< /hint >}}

Willkommen auf meiner persönlichen Wissensdatenbank zu Themen der
Informationstechnologie, welche mich zu einem bestimmten Zeitpunkt
beschäftigt haben. Die Informationen sind unter sehr pragmatischen
Gesichtspunkten gesammelt worden. Sie weisen zahlreiche Lücken auf und sind
in vielen Fällen auch nicht mehr aktuell.

## Sektionen

### Bibliotheken

Nicht sprachspezifische Bibliotheken bzw. Bibliotheken mit APIs für diverse Programmiersprachen

### Konzepte

Generelle Konzepte der IT

### Programme

Anleitungen zu Anwenderprogrammen

### Sprachen

Programmier- und Abfragesprachen 

### Standards

Sprachagnostische Standards

### Verfahren

Sprachagnostische Verfahren
