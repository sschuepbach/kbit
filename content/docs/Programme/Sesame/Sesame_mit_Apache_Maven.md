+++
title = 'Sesame mit Apache Maven und IntelliJ IDEA'
date = '2015-03-05'
+++

# Sesame mit Apache Maven und IntelliJ IDEA

## Initialisierung eines neuen Projekts

### Neues Projekt öffnen
![](./Sesame_mit_Apache_Maven/pasted_image.png)

### Maven-Projekt auswählen

1. Project SDK auf den root-Ordner der zu benützenden Java-Version legen
2. Create from archetype abwählen

![](./Sesame_mit_Apache_Maven/pasted_image001.png)

### POM-Deklarationen
Allgemeine Informationen zur POM sind auf der [Website von Apache Maven](http://maven.apache.org/guides/introduction/introduction-to-the-pom.html) zu finden.

1. GroupId: Die ID der Projektgruppe
2. ArtifactId: Die ID des Projekts
3. Version: Versionsnummer des Projekts

![](./Sesame_mit_Apache_Maven/pasted_image002.png)

### Projektordner
![](./Sesame_mit_Apache_Maven/pasted_image003.png)

### Generisches POM
Die obigen Angaben resultieren in einem "generischen" POM:
![](./Sesame_mit_Apache_Maven/pasted_image008.png)

Darüber hinaus können noch eine ganze Reihe weiterer Elemente definiert werden, bspw. name für den Projekttitel oder description für einen Projektbeschrieb.

### Autoimport von Libraries aktivieren
Noch ist nur eine kleine Anzahl von Libraries in das Projekt eingebunden. Um weitere zu importieren (bspw. Sesame-Bibliotheken), müssen in der POM zusätzliche Abhängigkeiten (Dependencies) festgelegt werden. Zuvor aber ist aber es sinnvoll, die Option Autoimport von Libraries zu aktivieren:
*FIle > Settings > Build, Execution, Deployment > Build Tools > Maven > Importing > Import Maven projects automatically*
![](./Sesame_mit_Apache_Maven/pasted_image007.png)

### Import der Sesame-Bibliotheken
Es besteht die Möglichkeit, das ganze Sesame-Framework in das Projekt zu integrieren. [In vielen Szenarien reicht aber ein Teil der Bibliotheken aus](http://rdf4j.org/sesame/2.8/docs/programming.docbook?view#chapter-lib-install). Abhängigkeiten können folgendermassen in der POM deklariert werden (hier wird das ganze Sesame-Framework importiert):
![](./Sesame_mit_Apache_Maven/pasted_image010.png)

