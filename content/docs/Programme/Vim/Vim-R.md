+++
title = 'Vim-R'
date = '2015-08-17'
lastmod = '2021-10-17'
+++

# Vim-R

## Starten / Beenden

```Bash
tmux
vim --servername VIM filename
exit
```
