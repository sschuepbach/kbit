+++
title = 'JVM-Management'
+++

 JVM-Management
Es gibt eine Reihe von Tools, welche bei der Administration / Überwachung von Applikationen helfen, die in einer JVM laufen:

* ``jps``: Lists the instrumented JVMs on the target system.
* ``jstat``: Monitors JVM statistics.
* ``jmap``: Prints shared object memory maps or heap memory details of a process, core file, or remote debug server


## Ressourcen

* [Java Memory Management for Java Virtual Machine (JVM)](https://betsol.com/2017/06/java-memory-management-for-java-virtual-machine-jvm/)


