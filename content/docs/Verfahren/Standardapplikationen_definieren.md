+++
title = 'Standardapplikationen definieren'
+++

# Standardapplikationen definieren
@Systemadministration

Um die Standardapplikation für einen Dateityp zu ändern, über die Konsole
``xdg-mime default application.desktop mime/type``
eingeben.

