+++
title = 'Text as Data: The Promise and Pitfalls of Automatic Content Analysis Methods for Political Texts'
+++

# Text as Data: The Promise and Pitfalls of Automatic Content Analysis Methods for Political Texts
"We emphasize that the complexity of language implies that automated content analysis methods will never replace careful and close reading of texts. Rather, the methods that we profile here are best thought of as amplifying and augmenting careful reading and thoughtful analysis. Further, automated content methods are incorrect models of language. This means that the performance of any one method on a new data set cannot be guaranteed, and therefore validation is essential when applying automated content methods. We describe best practice validations across diverse research objectives and models." (268)

Texte werden grundsätzlich auf zwei Arten weiterverarbeitet:

* Classification: Organisiert Texte in ein Set von bekannten oder noch unbekannten Kategorien
* Scaling:
	* Word scores: "(...) relies on guidance from reference texts to situate other political actors in a space" (269)
	* Word fish: "(...) exploits an assumption about how ideology affects word usage" (269)


Vier Prinzipien der automatisierten Textanalyse:

* Alle quantitativen Sprachmodelle sind falsch - aber einige sind nützlich
* Quantitative Methoden unterstützen Forschende, ersetzen sie nicht
* Es gibt keine generell beste Methode für automatisierte Textanalyse
* Validieren, validieren, validieren


Reduktion der Komplexität
# Text als _bag of words_: Es wird eine Liste der Wörter erstellt (_unigrams_), in Ausnahmefälle als _bigrams_ oder _trigrams_. "In practice, for common tasks like measuring sentiment, topic modeling, or search, _n-grams_ do little to enhance performance (...)." (272)
# _Stemming_: "Stemming removes the ends of words to reduce the total number of unique words in the data set, or reduce the _dimensionality_ of text. Stemming reduces the complexity by mapping words that refer to the same basic concept to a single root. For example, `family`, `families`, `families'`, and `familial` all become _famili_." (272) Stemming ist eine Annäherung an das linguistische Konzept der _Lemmatisierung_.
# Ausscheiden von Stoppwörtern (Satzzeichen und alle Wörter, die beispielsweise in mehr als 99% oder weniger als 1% der Dokumente vorkommen)
# _Document term matrix_: Eine Kollektion von Vektoren, wobei jeder Vektor die Anzahl jedes relevanten Wortes _pro Dokument_ beinhaltet, das im _Dokumentenkorpus_ vorkommt (d.h., in den Vektoren ist für viele Wörter die Anzahl null (Seltenheit (_Sparsity_)).

Die _Dictionary_-Methode, eine Unterkategorie von Klassifizierung, benutzt das relative Auftreten von Schlüsselwörter, um über das Vorhandensein einer bestimmten Kategorie in einem Text zu befinden. "A dictionary to measure tone is a list of words that are either dichotomously classified as positive or negative or contain more continuous measures of their content." (274) 

